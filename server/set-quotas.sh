#!/usr/bin/env sh

# Run as root or with sudo

while read user minq maxq
do
	if setquota -u $user $minq $maxq 0 0 /home
	then
		echo "Quota set for $user: min=$minq max=$maxq"
	else
		echo "Failed to set quota for $user"
	fi
done

