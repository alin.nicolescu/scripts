#!/usr/bin/env sh

# Run as root or with sudo

while read user
do
	if mkdir "/home/$user/public_html"
    then
		echo "Created /home/$user/public_html directory"
		if chown -R "$user":"$user" "/home/$user/public_html/"
		then
			echo "Applyed $user:$user ownership to the directory"
		else
			echo "Failed to apply $user:$user to the directory"
		fi
	else
		echo "Failed to create /home/$user/public_html directory"
	fi
done
