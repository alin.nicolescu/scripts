#!/usr/bin/env bash

# Get a list of normal users

UID_MIN=$(awk '/^UID_MIN/  {print $2}' /etc/login.defs)
UID_MAX=$(awk '/^UID_MAX/  {print $2}' /etc/login.defs)

awk -v a=$UID_MIN -v b=$UID_MAX \
    'BEGIN {FS=":"}{if($3>=a && $3<=b) print $1}' /etc/passwd

