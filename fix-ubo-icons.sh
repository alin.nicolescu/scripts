#!/usr/bin/env bash

# Fix for missing icons in Ubo
# Run from Ubo icon theme main folder.

for DIR in "$(pwd)"/*
do
    echo "Processing $DIR"
    if [ -d "$DIR" ]; then
        cd "$DIR/places" || exit
        cp -v folder-sound.png folder-music.png
        cp -v folder-image.png folder-pictures.png
        cp -v folder-video.png folder-videos.png
        cp -v folder.png folder-templates.png
        cd ../..
    fi
done
