# Scripts

Not all are presented below.

## `newpr`

I use this to create `beamer`-like `PDF` presentations from a markdown file
using `pandoc`.

The `newpr project` script creates a folder named `project` with two files
in it:

- `project.md` which contains a template of the presentation with a `YAML` header
- `Makefile`

Run `make` from within the `project` folder to generate the `PDF` file or
`make clean` to remove it.

## `newart`

I use this to create `PDF` `LaTeX`-style articles from a markdown file using
`pandoc`.

Works just like `newpr` script.

## `suck*` scripts and `fullclean`

Based on [uoou](https://gitlab.com/uoou/) scripts.

## `git-user-config.sh`

Set user options (name, email, gpg key) for a  `git` repo (local options).

## `git-nosign.sh`

Disable `git` signing option.

## `create-sql-users.sh`

This script batch creates users for the `MySQL/MariaDB` server. For each user
a database with the **same name** as the user is created. Users will have full
access to their database.

Create a simple **text file** and place on each line pairs like
`user:user-password`:

```
joe:password1
mary:password2
bob:password3
```

and then run as `root`:

```sh
./create-sql-users.sh < path/to/text-file

```

## `create-user-dirs.sh`

Use this script to batch create `public_html` folders for a list of users.

Create a simple **text file** and place in it a user name on each line and
then run:

```sh
./create-user-dirs.sh < path/to/users-file
```

## `set-quotas.sh`

Use this script to batch set minimum and maximum *quota limits* for a list of
users.

Create a simple **text file** and place there, on each line, the user name and
the min and max quota limits like this:

```
joe 100M 150M
alice 200M 250M
bob 300M 400M
```

and then run:

```sh
./set-quotas.sh < path/to/text-file
```
