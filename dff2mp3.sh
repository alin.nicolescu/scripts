#!/bin/bash
# Convert all dff files from the current folder to mp3 format
for file in *.dff
do
    ffmpeg -i "$file" -c:a mp3 "${file%.dff}".mp3
done

